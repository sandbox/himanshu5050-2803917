<?php

namespace Drupal\field_aggregate\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'CustomFieldFormatter' formatter.
 *
 * @FieldFormatter(
 *   id = "aggregate_formatter",
 *   label = @Translation("Aggregate Formatter"),
 *   field_types = {
 *     "aggregate",
 *   }
 * )
 */
class AggregateFieldFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $summary[] = t( 'Displays the random string.' );

    return $summary;
  }


  /**
   * Define how the field type is showed.
   *
   * Inside this method we can customize how the field is displayed inside
   * pages.
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $elements = [];
    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#type' => 'markup',
        '#markup' => $item->service_type,
      ];
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        // Declare a setting named 'text_length', with
        // a default value of 'short'
        'text_length' => 'short',
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = [];
    $element['text_length'] = [
      '#title' => t( 'Text length' ),
      '#type' => 'select',
      '#options' => [
        'short' => $this->t( 'Short' ),
        'long' => $this->t( 'Long' ),
      ],
      '#default_value' => $this->getSetting( 'text_length' ),
    ];

    return $element;
  }

} // class
