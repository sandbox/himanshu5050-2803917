<?php

namespace Drupal\field_aggregate\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface as StorageDefinition;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'address' field type.
 *
 * @FieldType(
 *   id = "aggregate",
 *   label = @Translation("Aggregate"),
 *   description = @Translation("Aggregate Restful Service API Response to the
 *   Field"), category = @Translation("Reference"), default_widget =
 *   "aggregate_widget", default_formatter = "aggregate_formatter"
 * )
 */
class AggregateFieldType extends FieldItemBase {

  /**
   * Field type properties definition.
   *
   * Inside this method we defines all the fields (properties) that our
   * aggregate field type will have.
   *
   * Here there is a list of allowed property types: https://goo.gl/sIBBgO
   */
  public static function propertyDefinitions(StorageDefinition $storage) {

    $properties = [];

    $properties ['list_service_entities'] = DataDefinition::create( 'string' );

    return $properties;
  }

  /**
   * Field type schema definition.
   *
   * Inside this method we defines the database schema used to store data for
   * our field type.
   *
   * Here there is a list of allowed column types: https://goo.gl/YY3G7s
   */
  public static function schema(StorageDefinition $storage) {

    $columns = [];

    $columns['list_service_entities'] = [
      'type' => 'text',
      'size' => 'tiny',
      'not null' => FALSE,
    ];

    return [
      'columns' => $columns,
      'indexes' => [],
    ];
  }

  /**
   * Define when the field type is empty.
   *
   * This method is important and used internally by Drupal. Take a moment
   * to define when the field type must be considered empty.
   */
  public function isEmpty() {

    $value = $this->get( 'list_service_entities' )->getValue();

    return $value === NULL || $value === '';
  }

  /**
   *
   * {@inheritdoc}
   *
   */
  public static function defaultFieldSettings() {
    return [
        // Declare a single setting, 'size', with a default
        // value of 'large'
        'list_service_entities' => 'none',
      ] + parent::defaultFieldSettings();
  }

  /**
   *
   * {@inheritdoc}
   *
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];
    $options = [];

    $options = $this->getAllListEntities( 'service_entity' );

    // The key of the element should be the setting name
    $elements ['list_service_entities'] = [
      '#title' => $this->t( 'Select Service Entity:' ),
      '#description' => $this->t( 'Select for which service entity, restful service API response will be aggregate into the field' ),
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => $this->getSetting( 'list_service_entities' ),
    ];

    return $elements;
  }

  public static function getAllListEntities($entity) {
    $options = [];
    // Query for getting all service entity ids.
    $query = \Drupal::entityQuery( $entity );
    $query->condition( 'status', 1 );
    $entity_ids = $query->execute();

    $services = \Drupal::entityTypeManager()
      ->getStorage( $entity )
      ->loadMultiple( $entity_ids );

    foreach ($services as $service) {
      $options[$service->id()] = $service->label();
    }

    return $options;
  }


} // class
