<?php

namespace Drupal\field_aggregate\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'CustomFieldWidget' widget.
 *
 * @FieldWidget(
 *   id = "aggregate_widget",
 *   label = @Translation("Aggregate Widget"),
 *   field_types = {
 *     "aggregate",
 *   }
 * )
 */
class AggregateFieldWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        // Create a default setting 'size', and
        // assign a default value of 60
        'size' => 200,
      ] + parent::defaultSettings();
  }


  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = [];
    $element['size'] = [
      '#type' => 'number',
      '#title' => t( 'Size of textfield' ),
      '#default_value' => $this->getSetting( 'size' ),
      '#required' => TRUE,
      '#min' => 1,
    ];

    return $element;
  }


  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $summary[] = t( 'Textfield size: @size', ['@size' => $this->getSetting( 'size' )] );

    return $summary;
  }


  /**
   * Define the form for the field type.
   *
   * Inside this method we can define the form used to edit the field type.
   *
   * Here there is a list of allowed element types: https://goo.gl/XVd4tA
   */
  public function formElement(
    FieldItemListInterface $items,
    $delta,
    Array $element,
    Array &$form,
    FormStateInterface $formState
  ) {

    $options = [];

    $options = \Drupal\field_aggregate\Plugin\Field\FieldType\AggregateFieldType::getAllListEntities( 'service_entity' );
    $options =

    $element['service_type'] = [
      '#type' => 'select',
      '#title' => t( 'Select Service Entity:' ),
      '#default_value' => isset( $items[$delta]->service_type ) ?
        $items[$delta]->service_type : NULL,
      '#options' => $options,
      '#empty_value' => '',
      '#placeholder' => t( 'Select Service Type' ),
    ];

    return $element;
  }

} // class
