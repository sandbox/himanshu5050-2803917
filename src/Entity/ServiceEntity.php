<?php

namespace Drupal\field_aggregate\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Service entity entity.
 *
 * @ConfigEntityType(
 *   id = "service_entity",
 *   label = @Translation("Service entity"),
 *   handlers = {
 *     "list_builder" = "Drupal\field_aggregate\ServiceEntityListBuilder",
 *     "form" = {
 *       "add" = "Drupal\field_aggregate\Form\ServiceEntityForm",
 *       "edit" = "Drupal\field_aggregate\Form\ServiceEntityForm",
 *       "delete" = "Drupal\field_aggregate\Form\ServiceEntityDeleteForm",
 *       "response" = "Drupal\field_aggregate\Form\ServiceEntityResponseForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\field_aggregate\ServiceEntityHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "service_entity",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "service_end_point" = "Service End Point",
 *     "description_value" = "Description"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/service_entity/{service_entity}",
 *     "add-form" = "/admin/structure/service_entity/add",
 *     "edit-form" = "/admin/structure/service_entity/{service_entity}/edit",
 *     "delete-form" =
 *   "/admin/structure/service_entity/{service_entity}/delete",
 *     "collection" = "/admin/structure/service_entity",
 *     "response" =
 *   "/admin/structure/service_entity/response/{service_entity}",
 *   }
 * )
 */
class ServiceEntity extends ConfigEntityBase implements ServiceEntityInterface {

  /**
   * The Service entity ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Service entity label.
   *
   * @var string
   */
  protected $label;

}
