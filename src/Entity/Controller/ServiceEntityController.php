<?php
/**
 * @file
 * \Drupal\field_aggregate\Entity\Controller\ServiceEntityController
 */

namespace Drupal\field_aggregate\Entity\Controller;

use Drupal\Core\Entity\Controller\EntityController;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\RouteMatchInterface;

class ServiceEntityController extends EntityController {

  /**
   * Provides a generic edit title callback.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\Core\Entity\EntityInterface $_entity
   *   (optional) An entity, passed in directly from the request attributes.
   *
   * @return string|null
   *   The title for the entity edit page, if an entity was found.
   */
  public function viewTitle(RouteMatchInterface $route_match, EntityInterface $_entity = NULL) {
    if ($entity = \Drupal\Core\Entity\Controller\EntityController::doGetEntity( $route_match, $_entity )) {
      return $this->t( 'View %label Response', ['%label' => $entity->label()] );
    }
  }
}
