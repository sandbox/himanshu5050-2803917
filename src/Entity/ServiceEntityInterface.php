<?php

namespace Drupal\field_aggregate\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Service entity entities.
 */
interface ServiceEntityInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
