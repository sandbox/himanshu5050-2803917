<?php

namespace Drupal\field_aggregate;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of Service entity entities.
 */
class ServiceEntityListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [];
    $header['label'] = $this->t( 'Service Name' );
    $header['id'] = $this->t( 'Machine name' );
    $header['description'] = $this->t( 'Description' );

    //$header['service_end_point'] = $this->t('End Point');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row = [];
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $row['description'] = $entity->get( 'description_value' );
    //$row['service_end_point'] = $entity->get('service_end_point');
    // You probably want a few more properties here...
    return $row + parent::buildRow( $entity );
  }

  public function getOperations(EntityInterface $entity) {
    $operations = parent::getOperations( $entity );
    $operations['response'] = [
      'title' => $this->t( 'Response' ),
      'weight' => 20,
      'url' => $entity->urlInfo( 'response' ),
    ];

    return $operations;
  }

}
