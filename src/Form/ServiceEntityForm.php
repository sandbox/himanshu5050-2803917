<?php

namespace Drupal\field_aggregate\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CssCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ServiceEntityForm.
 *
 * @package Drupal\field_aggregate\Form
 */
class ServiceEntityForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form( $form, $form_state );
    $service_entity = $this->entity;


    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t( 'Label' ),
      '#maxlength' => 255,
      '#size' => 30,
      '#default_value' => $service_entity->label(),
      '#description' => $this->t( "Label for the Service entity." ),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $service_entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\field_aggregate\Entity\ServiceEntity::load',
      ],
      '#disabled' => !$service_entity->isNew(),
    ];


    // Create service API basic information fieldset.
    $form['basic_service_information'] = [
      '#type' => 'fieldset',
      '#title' => $this->t( 'Service Basic Information' ),
    ];


    $form['basic_service_information']['need_description'] = [
      '#type' => 'checkbox',
      '#title' => $this->t( 'Description' ),
    ];

    $form['basic_service_information']['description'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => 'api',
      ],
      '#states' => [
        'invisible' => [
          'input[name="need_description"]' => ['checked' => FALSE],
        ],
      ],
    ];

    $form['basic_service_information']['description']['description_value'] = [
      '#type' => 'textfield',
      '#title' => $this->t( '' ),
      //'#required' => TRUE,
      '#size' => 60,
      '#default_value' => $service_entity->get( 'description_value' ),
      '#description' => $this->t( "Add description associate with the Service Entity." ),
    ];


    $form['basic_service_information']['service_end_point'] = [
      '#type' => 'url',
      '#title' => $this->t( 'Service End Point' ),
      '#maxlength' => 255,
      '#default_value' => $service_entity->get( 'service_end_point' ),
      '#description' => $this->t( "Endpoint for the Service." ),
      '#required' => TRUE,
    ];


    // Create service API settings information fieldset.
    $form['service_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t( 'Service Parameter Settings' ),
    ];

    $form['service_settings']['need_api'] = [
      '#type' => 'checkbox',
      '#title' => $this->t( 'API Key Required?' ),
    ];

    $form['service_settings']['api'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => 'api',
      ],
      '#states' => [
        'invisible' => [
          'input[name="need_api"]' => ['checked' => FALSE],
        ],
      ],
    ];

    $form['service_settings']['api']['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t( 'API Key' ),
      //'#required' => TRUE,
      '#default_value' => $service_entity->label(),
      '#description' => $this->t( "Add API Key associate with the Service Entity." ),
    ];


    // Add Parameter.
    $form['service_settings']['need_parameters'] = [
      '#type' => 'checkbox',
      '#title' => $this->t( 'Parameters Required?' ),
    ];

    $form['service_settings']['parameters'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => 'parameter',
      ],
      '#states' => [
        'invisible' => [
          'input[name="need_parameters"]' => ['checked' => FALSE],
        ],
      ],
    ];


    $form['service_settings']['parameters']['information'] = [
      '#type' => 'vertical_tabs',
      '#default_tab' => 'edit-publication',
    ];

    $form['service_settings']['keys'] = [
      '#type' => 'details',
      '#title' => $this->t( 'Keys' ),
      '#group' => 'information',
    ];

    $form['service_settings']['keys']['key'] = [
      '#type' => 'textfield',
      '#title' => $this->t( 'Enter Keys' ),
      '#default_value' => $service_entity->label(),
      '#description' => $this->t( "Add API Keys associate with the Service Entity, Separated by ','." ),
    ];

    $form['service_settings']['values'] = [
      '#type' => 'details',
      '#title' => $this->t( 'Values' ),
      '#group' => 'information',
    ];

    $form['service_settings']['values']['value'] = [
      '#type' => 'textfield',
      '#title' => $this->t( 'Enter Values' ),
      '#default_value' => $service_entity->label(),
      '#description' => $this->t( "Add API values corresponding to the keys, seperated by ','." ),
    ];


    // Create service API response information fieldset.
    $form['service_response_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t( 'Service Response Settings' ),
    ];


    $form['service_response_settings']['response_format'] = [
      '#type' => 'radios',
      '#title' => $this->t( 'Service Response Format' ),
      '#default_value' => 0,
      '#options' => ['json' => $this->t( 'JSON' ), 'xml' => $this->t( 'XML' )],
      '#required' => TRUE,
      '#ajax' => [
        'callback' => [$this, 'validateServiceEntityAjax'],
        'event' => 'change',
        'progress' => [
          'type' => 'throbber',
          'message' => t( 'Verifying Service Entity Response...' ),
        ],
      ],
      '#suffix' => '<span class="email-valid-message"></span>',
    ];


    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * Ajax callback to validate the email field.
   */
  public function validateServiceEntityAjax(array &$form, FormStateInterface $form_state) {
    $valid = $this->validateServiceResponse( $form, $form_state );
    $response = new AjaxResponse();
    if ($valid) {
      $css = ['border' => '1px solid green'];
      $message = $this->t( 'Validation Test Passed, data returned in object form' );
    }
    else {
      $css = ['border' => '1px solid red'];
      $message = $this->t( 'validation not passed , you can not save this service entity.' );
      $valid_service_entity = FALSE;
    }
    $response->addCommand( new CssCommand( '#edit-email', $css ) );
    $response->addCommand( new HtmlCommand( '.email-valid-message', $message ) );

    return $response;
  }

  /**
   * Validates that the email field is correct.
   */
  protected function validateServiceResponse(array &$form, FormStateInterface $form_state) {
    // Cache Parameter values.
    $label = $form_state->getValue( 'label' );
    $service_url = $form_state->getValue( 'service_url' );
    $response_format = $form_state->getValue( 'response_format' );
    $need_api = $form_state->getValue( 'need_api' );
    $api_key = ($need_api) ? $form_state->getValue( 'api_key' ) : '';
    //$need_parameters = $form_state->getValue('need_parameters');
    //$keys = ($need_parameters) ? $form_state->getValue('keys'):'';
    //$values = ($need_parameters) ? $form_state->getValue('values'):'';
    $url = $service_url . '/?api_key=' . $api_key;

    switch ($response_format) {
      case 'json':
        return $this->validateJsonResponse( $url );
        break;
      case 'xml':
        return $this->validateXmlResponse( $url );
        break;
      default:
        return FALSE;
    }
  }

  protected function validateJsonResponse($url) {
    $url = 'http://iframe.ly/api/iframely?url=http%3A%2F%2Fvimeo.com%2F62092214&api_key=a9b61e09410f1241e2f2a5';
    $json = file_get_contents( $url );
    $obj = json_decode( $json );
    if ($obj) {
      return $obj;
    }
    else {
      return FALSE;
    }
  }

  protected function validateXmlResponse($url) {
    // Function is not implemented yet.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    // @todo Consider renaming the action key from submit to save. The impacts
    //   are hard to predict. For example, see
    //   \Drupal\language\Element\LanguageConfiguration::processLanguageConfiguration().

    $actions['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t( 'Save Service' ),
      '#submit' => ['::submitForm', '::save'],
    ];

    if (!$this->entity->isNew() && $this->entity->hasLinkTemplate( 'delete-form' )) {
      $route_info = $this->entity->urlInfo( 'delete-form' );
      if ($this->getRequest()->query->has( 'destination' )) {
        $query = $route_info->getOption( 'query' );
        $query['destination'] = $this->getRequest()->query->get( 'destination' );
        $route_info->setOption( 'query', $query );
      }
      $actions['delete'] = [
        '#type' => 'link',
        '#title' => $this->t( 'Delete' ),
        '#access' => $this->entity->access( 'delete' ),
        '#attributes' => [
          'class' => ['button', 'button--danger'],
        ],
      ];
      $actions['delete']['#url'] = $route_info;
    }

    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $service_entity = $this->entity;
    $status = $service_entity->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message( $this->t( 'Created the %label Service entity.', [
          '%label' => $service_entity->label(),
        ] ) );
        break;

      default:
        drupal_set_message( $this->t( 'Saved the %label Service entity.', [
          '%label' => $service_entity->label(),
        ] ) );
    }
    $form_state->setRedirectUrl( $service_entity->urlInfo( 'collection' ) );

  }

}
