<?php

namespace Drupal\field_aggregate\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ServiceEntityForm.
 *
 * @package Drupal\field_aggregate\Form
 */
class ServiceEntityResponseForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form( $form, $form_state );
    $service_entity = $this->entity;

    // Create service API basic information fieldset.
    $form['service_response_information'] = [
      '#type' => 'fieldset',
      '#title' => $this->t( 'Service Response Information' ),
    ];


    $form['service_response_information']['service_end_point'] = [
      '#type' => 'textfield',
      '#title' => $this->t( 'Service End Point' ),
      '#maxlength' => 255,
      '#size' => 30,
      '#default_value' => $service_entity->get( 'service_end_point' ),
      '#description' => $this->t( "Service End Point" ),
      '#required' => TRUE,
      '#disabled' => 'disabled',
    ];

    $response_array = $this->getResponse( $service_entity->get( 'service_end_point' ) );


    $form['service_response_information']['response_value'] = [
      '#type' => 'textarea',
      '#title' => $this->t( 'Response' ),
      '#value' => $response_array,
      '#required' => TRUE,
      '#disabled' => 'disabled',
    ];

    return $form;
  }


  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    // @todo Consider renaming the action key from submit to save. The impacts
    //   are hard to predict. For example, see
    //   \Drupal\language\Element\LanguageConfiguration::processLanguageConfiguration().

    $actions = [];
    $actions['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t( 'Back' ),
      '#submit' => ['::submitForm', '::save'],
    ];

    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $service_entity = $this->entity;
    $form_state->setRedirectUrl( $service_entity->urlInfo( 'collection' ) );

  }

  protected function getResponse($service_end_point) {
    $response = "Display response returned by service  " . $service_end_point . "  here...";

    return $response;
  }

}
